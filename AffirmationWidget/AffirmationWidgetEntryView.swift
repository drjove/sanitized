//
//  AffirmationWidgetEntryView.swift
//  Daily Affirmation (iOS)
//
//  Created by David Giovannini on 4/12/21.
//

import WidgetKit
import SwiftUI

struct AffirmationWidgetEntryView : View {
	@Environment(\.widgetFamily) var widgetFamily
    var entry: AffirmationWidgetProvider.Entry

    var body: some View {
		ZStack(alignment: .center) {
			if widgetFamily == .systemMedium {
				AffirmationDetailView(affirmation: entry.affirmation)
			}
			else {
				AffirmationStampView(affirmation: entry.affirmation)
			}
        }
        .padding(.all, 8)
        .widgetURL(URL(string: "\(AffirmingNotification.scheme)://tap"))
    }
}

struct AffirmationWidgetEntryView_Previews: PreviewProvider {
    static var previews: some View {
		ForEach([WidgetFamily.systemSmall, WidgetFamily.systemMedium], id: \.rawValue) {
			AffirmationWidgetEntryView(entry: AffirmingNotification())
		.previewContext(WidgetPreviewContext(family: $0))
		}
    }
}
