//
//  AffirmationWidgetProvider.swift
//  AffirmationWidgetExtension
//
//  Created by David Giovannini on 4/12/21.
//

import WidgetKit

extension AffirmingNotification: TimelineEntry {
}

struct AffirmationWidgetProvider: TimelineProvider {
    func placeholder(in context: Context) -> AffirmingNotification {
        return AffirmingNotification(affirmation: Affirmation.redacted)
    }

    func getSnapshot(in context: Context, completion: @escaping (AffirmingNotification) -> ()) {
		let affirmCalendar = AffirmationCalendar(verify: true)
        completion(affirmCalendar.docket.today)
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<AffirmingNotification>) -> ()) {
		let affirmCalendar = AffirmationCalendar(verify: true)
		let docket = affirmCalendar.docket
		let today = docket.today
		let tomorrow = docket.tomorrow
		let end = Calendar.current.date(byAdding: .day, value: 1, to: tomorrow.date)!
        let timeline = Timeline(entries: [today, tomorrow], policy: .after(end))
        completion(timeline)
    }
}
