//
//  AffirmationWidget.swift
//  AffirmationWidget
//
//  Created by David Giovannini on 4/12/21.
//

import WidgetKit
import SwiftUI

@main
struct AffirmationWidget: Widget {
    var body: some WidgetConfiguration {
		StaticConfiguration(kind: AffirmingNotification.kind, provider: AffirmationWidgetProvider()) { entry in
            AffirmationWidgetEntryView(entry: entry)
        }
        .supportedFamilies([.systemSmall, .systemMedium])
        .configurationDisplayName("Affirmation Widget")
        .description("Daily affirmation.")
    }
}
