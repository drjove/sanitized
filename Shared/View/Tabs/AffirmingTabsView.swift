//
//  AffirmingTabsView.swift
//  Daily Affirmation (iOS)
//
//  Created by David Giovannini on 4/5/21.
//

import SwiftUI

struct AffirmingTabsView: View {
	@ObservedObject var session: AffirmationSession
	@ObservedObject var notifications: AffirmingNotificationIssuer
    @Binding var presentingStartAgain: Bool
    @Binding var selectedTab: Int
    var body: some View {
		TabView(selection: $selectedTab) {
			VStack {
				AffirmingFullyView(fully: $session.fully)
				Button("startAgain") {
					self.presentingStartAgain = true
				}
			}
			.padding(4)
			.tabItem() {
				Image(systemName: "list.bullet.rectangle")
				Text("today")
			}
			.tag(0)
			
			HistoryView(session: session)
			.padding(4)
			.tabItem() {
				Image(systemName: "hourglass")
				Text("history")
			}
			.tag(1)
			
			SettingsView(session: session, notifications: notifications, selectedTab: $selectedTab)
			.padding(4)
			.tabItem() {
				Image(systemName: "gear")
				Text("settings")
			}
			.tag(2)
			
			AboutView()
			.padding(4)
			.tabItem() {
				Image(systemName: "info.circle")
				Text("about")
			}
			.tag(3)
		}
	}
}

struct AffirmingTabsView_Previews: PreviewProvider {
    static var previews: some View {
		AffirmingTabsView(session: AffirmationSession(), notifications: AffirmingNotificationIssuer(), presentingStartAgain: .constant(false), selectedTab: .constant(0))
    }
}
