//
//  AffirmationLoadingView.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/5/21.
//

import SwiftUI

struct AffirmationLoadingView: View {
	@ObservedObject var session: AffirmationSession
	@ObservedObject var notifications: AffirmingNotificationIssuer
    @State private var presentingStartAgain = false
    @Binding var selectedTab: Int
    
    init(session: AffirmationSession, notifications: AffirmingNotificationIssuer, selectedTab: Binding<Int>) {
		self.session = session
		self.notifications = notifications
		self.presentingStartAgain = session.newDayTrigger
		self._selectedTab = selectedTab
    }
    
    var body: some View {
		StartAgainAlerts(session, $presentingStartAgain) {
			if session.loading == false {
				AffirmingTabsView(session: session, notifications: notifications, presentingStartAgain: $presentingStartAgain, selectedTab: $selectedTab)
			}
			else {
				ProgressView()
			}
		}
    }
}

fileprivate struct StartAgainAlerts<Content> : View where Content : View {
	@ObservedObject var session: AffirmationSession
    @Binding var presentingStartAgain: Bool
	let content: Content
    
    init(_ session: AffirmationSession, _ presentingStartAgain: Binding<Bool>, @ViewBuilder content: () -> Content) {
		self.session = session
		self._presentingStartAgain = presentingStartAgain
		self.content = content()
    }
	
    var body: some View {
		content
		.onChange(of: session.newDayTrigger) { newDay in
			self.presentingStartAgain = session.newDayTrigger
		}
		.alert(isPresented: $presentingStartAgain) {
			if session.newDayTrigger {
				return Alert(
					title: Text("newDay"),
					message: Text("continueOrStartAgain"),
					primaryButton: .default(Text("startNewDay")) {
						self.session.startAgain(agreed: true, resetPrime: false)
					},
					secondaryButton: .cancel() {
						self.session.startAgain(agreed: false, resetPrime: false)
					}
				)
			}
			else if session.fully.isToday {
				return Alert(
					title: Text("clearQuestion"),
					message: Text("noUndo"),
					primaryButton: .destructive(Text("clear")) {
						self.session.startAgain(agreed: true, resetPrime: true)
					},
					secondaryButton: .cancel() {
						self.session.startAgain(agreed: false, resetPrime: false)
					}
				)
			}
			else {
				return Alert(
					title: Text("doneQuestion"),
					message: Text("noGoingBack"),
					primaryButton: .default(Text("newDay")) {
						self.session.startAgain(agreed: true, resetPrime: false)
					},
					secondaryButton: .cancel() {
						self.session.startAgain(agreed: false, resetPrime: false)
					}
				)
			}
		}
    }
}

struct AffirmationLoadingView_Previews: PreviewProvider {
    static var previews: some View {
		return AffirmationLoadingView(session: AffirmationSession(), notifications: AffirmingNotificationIssuer(), selectedTab: .constant(0))
    }
}
