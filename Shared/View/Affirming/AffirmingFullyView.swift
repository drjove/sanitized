//
//  AffirmingFullyView.swift
//  Shared
//
//  Created by David Giovannini on 3/30/21.
//

import SwiftUI

struct AffirmingFullyView: View {
	@Binding var fully: AffirmingFully
	
	var body: some View {
		VStack {
			AffirmingTodayView(today: $fully.today)
			AffirmingListView(list: $fully.list)
		}
    }
}

struct AffirmingFullyView_Previews: PreviewProvider {
    static var previews: some View {
		AllColorSchemesView {
			AffirmingFullyView(fully: .constant(AffirmingFully(maxAffirmations: 5)))
		}
    }
}
