//
//  AffirmingEntryView.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/31/21.
//

import SwiftUI
import AVFoundation
 
struct AffirmingEntryView: View {
	@State private var synthesizer = AVSpeechSynthesizer()
	@Binding var entry: AffirmingEntry
	
	var body: some View {
		HStack(alignment: .center, spacing: 4) {
			AffirmationLineItemView(affirmation: entry.affirmation)
			.onTapGesture {
				let utterance = AVSpeechUtterance(string: entry.affirmation.title)
				utterance.rate = 0.5
				synthesizer.speak(utterance)
			}
			Spacer()
			EmotionMenu(emotion: $entry.emotion)
			SheetButton($entry, isEmpty: \.thoughts.isEmpty) {
				AffirmingEntryDetailView(entry: $0, isPresented: $1)
			}
		}
	}
}

struct AffirmingEntryView_Previews: PreviewProvider {
    static var previews: some View {
		AllColorSchemesView {
			List(AffirmCategory.allCases, id: \.self) { c in
				if let a = Affirmation.allCases.first(where: {$0.category==c}) {
					AffirmingEntryView(entry: .constant(AffirmingEntry(a)))
				}
			}
		}
    }
}
