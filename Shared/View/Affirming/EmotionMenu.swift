//
//  EmotionMenu.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/31/21.
//

import SwiftUI

struct EmotionMenu: View {
	@Binding var emotion: Emotion?
	
	var body: some View {
		Menu {
			ForEach(Emotion.allCases, id: \.self) { emotion in
				Button("\(emotion.emoticon) \(emotion.title)") {
					self.emotion = emotion
				}
			}
		}
		label: {
			Text(self.emotion?.emoticon ?? Emotion.enspecifiedEmoticon)
			.frame(minWidth: 22, minHeight: 44)
		}
	}
}

struct EmotionMenu_Previews: PreviewProvider {
    static var previews: some View {
		AllColorSchemesView {
			EmotionMenu(emotion: .constant(Emotion.angry))
		}
    }
}
