//
//  AffirmingEntryDetailView.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/31/21.
//

import SwiftUI

struct AffirmingEntryDetailView: View {
	@Binding var entry: AffirmingEntry
	@Binding var isPresented: Bool
	
    var body: some View {
		VStack(alignment: .leading) {
			HStack(alignment: .top) {
				AffirmationDetailView(affirmation: entry.affirmation)
				Spacer()
				VStack {
					Button("done") {
						self.isPresented = false
					}
					EmotionMenu(emotion: $entry.emotion)
				}
			}
			TextEditor(text: $entry.thoughts)
				.padding(4)
				.overlay(
					RoundedRectangle(cornerRadius: 8)
						.stroke(Color.accentColor, lineWidth: 1))
		}.padding(8)
    }
}

struct AffirmingEntryDetailView_Previews: PreviewProvider {
    static var previews: some View {
		AllColorSchemesView {
			AffirmingEntryDetailView(
				entry: Binding.constant(AffirmingEntry(Affirmation.allCases[0])),
				isPresented: Binding.constant(true))
		}
    }
}
