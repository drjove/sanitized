//
//  AffirmingTodayView.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/30/21.
//

import SwiftUI

struct AffirmingTodayView: View {
	@Binding var today: AffirmingToday
	
	private static let formatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateStyle = .long
		return formatter
	}()

	var body: some View {
		VStack(alignment: .center, spacing: 0) {
			Text("title").font(.title)
			Text(today.date, formatter: Self.formatter)
			Text(today.inspiration.description).fontWeight(.thin).multilineTextAlignment(.center)
				.padding(12)
			HStack {
				Text("todayQuestion")
				EmotionMenu(emotion: $today.emotion)
				SheetButton($today, isEmpty: \.thoughts.isEmpty) {
					AffirmingTodayDetailView(today: $0, isPresented: $1)
				}
			}
		}
	}
}

struct AffirmingTodayView_Previews: PreviewProvider {
    static var previews: some View {
		AllColorSchemesView {
			AffirmingTodayView(today: .constant(AffirmingToday()))
		}
    }
}
