//
//  AffirmingListView.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/30/21.
//

import SwiftUI

import Foundation

struct AffirmingListView: View {
	@Binding var list: AffirmingList
	var body: some View {
		List {
			Section(footer: AffirmingListFooter(list: $list)) {
				ForEach(list, id: \.1.affirmation.id) { index, _ in
					AffirmingEntryView(entry: self.$list[index].entry)
						.buttonStyle(PlainButtonStyle())
				}
			}
		}
    }
}

struct AffirmingListFooter : View {
	@Binding var list: AffirmingList
	var body: some View {
		if list.count < Affirmation.allCases.count {
			HStack {
				Spacer()
				Button("add") {
					list.add()
				}.foregroundColor(.accentColor)
				Spacer()
			}
		}
		else {
			EmptyView()
		}
	}
}

struct AffirmingListView_Previews: PreviewProvider {
    static var previews: some View {
		AllColorSchemesView {
			AffirmingListView(
				list: .constant(AffirmingList(max: 10)))
			}
    }
}
