//
//  AffirmingTodayDetailView.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/31/21.
//

import SwiftUI

struct AffirmingTodayDetailView: View {
	@Binding var today: AffirmingToday
	@Binding var isPresented: Bool
	
	var body: some View {
		VStack {
			HStack {
				Text("todayQuestion")
				EmotionMenu(emotion: $today.emotion)
				Spacer()
				Button("done") {
					self.isPresented = false
				}
			}
			Text(today.inspiration.description).fontWeight(.thin).multilineTextAlignment(.center)
				.padding(4)
			TextEditor(text: $today.thoughts)
				.padding(4)
				.overlay(
					RoundedRectangle(cornerRadius: 8)
						.stroke(Color.accentColor, lineWidth: 1))
		}.padding(8)
	}
}

struct AffirmingTodayDetailView_Previews: PreviewProvider {
    static var previews: some View {
		AllColorSchemesView {
			AffirmingTodayDetailView(
				today: .constant(AffirmingToday()),
				isPresented: .constant(true))
		}
    }
}
