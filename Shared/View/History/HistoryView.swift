//
//  HistoryView.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/5/21.
//

import SwiftUI

struct HistoryView: View {
	@ObservedObject var session: AffirmationSession
	@State private var emotionsGraph: [CGFloat?]
	
	init(session: AffirmationSession) {
		self.session = session
		self._emotionsGraph = State(initialValue: session.history.emotionsGraph())
	}

    let columns = [
		GridItem(.flexible(minimum: 110), spacing: 2, alignment: .leading),
        GridItem(.flexible(), spacing: 2, alignment: .center),
        GridItem(.flexible(), spacing: 2, alignment: .center),
        GridItem(.flexible(), spacing: 2, alignment: .center),
        GridItem(.flexible(), spacing: 2, alignment: .center),
        GridItem(.flexible(), spacing: 2, alignment: .center),
    ]
	
    var body: some View {
		VStack(spacing: 4) {
			Text("historyDays\(min(session.historyCount, session.historyCount))").bold()
			.onChange(of: session.history) { newValue in
                self.emotionsGraph = newValue.emotionsGraph()
            }
			HeatGraphCanvas(colors: Emotion.colors, dataPoints: self.emotionsGraph, yInterval: Emotion.allCases.count)
				.aspectRatio(16/9, contentMode: .fit)
				.padding()
			ScrollView(.vertical) {
				LazyVGrid(columns: columns, spacing: 20) {
					Text("")
					Text("Day's\n\(Emotion.historyEmoticon)")
						.multilineTextAlignment(.center)
					VStack(spacing: 0)  {
						Text("Day's")
						Image("Thought")
							.resizable()
							.scaledToFit()
							.frame(maxHeight: 20)
							.opacity(0.25)
					}
					Text("Affirm\n#")
						.multilineTextAlignment(.center)
					VStack(spacing: 0) {
						Text("Total")
						Text(Emotion.historyEmoticon)
					}
					VStack(spacing: 0) {
						Text("Total")
						Image("Thought")
							.resizable()
							.scaledToFit()
							.frame(maxHeight: 20)
							.opacity(0.25)
						}
					ForEach(session.history.reversed(), id: \.self) { entry in
						Text(entry.identifier).bold().lineLimit(1)
						Text(entry.dayEmotion?.emoticon ?? Emotion.enspecifiedEmoticon)
						Text(entry.dayWordCount.description)
						Text(entry.affirmationCount.description)
						Text(entry.entryEmotedCount.description)
						Text(entry.entryWordCount.description)
					}
				}
			}
		}
    }
}

struct HistoryView_Previews: PreviewProvider {
    static var previews: some View {
		HistoryView(session: AffirmationSession())
    }
}
