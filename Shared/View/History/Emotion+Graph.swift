//
//  Emotion+Graph.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/17/21.
//

import SwiftUI

extension Emotion {
	static var colors: HeatSpectrumColors {
		struct Colors: HeatSpectrumColors {}
		return Colors()
	}
	
	var rating: CGFloat {
		1.0 - CGFloat(self.index) / CGFloat(Self.allCases.count-1)
	}
}

extension Array
	where Element == AffirmingLogEntry {
	func emotionsGraph() -> [CGFloat?] {
		guard !self.isEmpty else { return [] }
		let cal = Calendar.current
		var nextDay: Date?
		var result: [CGFloat?] = []
		for i in 0..<self.count {
			let thisDay = self[i].identifier.todayIdentifierDate
			if let nextDay = nextDay {
				let numberOfDays = cal.dateComponents([.day], from: nextDay, to: thisDay).day!
				for _ in 0..<numberOfDays {
					result.append(nil)
				}
			}
			nextDay = cal.date(byAdding: .day, value: 1, to: thisDay)
			if let rating = self[i].dayEmotion?.rating {
				result.append(rating)
			}
			else {
				result.append(nil)
			}
		}
		return result
	}
}
