//
//  SettingsView.swift
//  Daily Affirmation (iOS)
//
//  Created by David Giovannini on 4/11/21.
//

import SwiftUI

extension AffirmingNotificationIssuer {
	var timeOfDayDate: Binding<Date> {
		Binding<Date>(
			get: {
				let timeOfDay = self.timeOfDay
				return Calendar.current.date(bySettingHour: timeOfDay.hour ?? 0, minute: timeOfDay.minute ?? 0, second: timeOfDay.second ?? 0, of: Date())!
			},
			set: {
				self.timeOfDay = Calendar.current.dateComponents([.hour, .minute], from: $0)
			}
		)
	}
}

struct SettingsView: View {
    @Environment(\.openURL) var openURL
    
	@ObservedObject var session: AffirmationSession
	@ObservedObject var notifications: AffirmingNotificationIssuer
	@State private var presentingDebugActionSheet: Bool = false
	@State private var presentingDebugView: Int = 0
    @Binding var selectedTab: Int
	
    var body: some View {
		Form {
			Section {
				HStack {
					Text("Settings")
					Spacer()
				}
			}
			.onLongPressGesture {
#if DEBUG
				self.presentingDebugView += 1
				if self.presentingDebugView == 4 {
					self.presentingDebugView = 0
				}
#endif
			}
			Stepper("affirmationCount\(session.maxAffirmations)", value: $session.maxAffirmations, in: 1...Affirmation.allCases.count)
			.frame(height:42)
			HStack {
				Text(notifications.active == true ?
					"notificationsEnabled" :
					"notificationsNotEnabled")
				Spacer()
				Button("Open Settings") {
					openURL(AffirmingNotificationIssuer.settingsURL)
				}
			}
			DatePicker("notificationTime", selection: notifications.timeOfDayDate, displayedComponents: .hourAndMinute)
				.datePickerStyle(CompactDatePickerStyle())
			.frame(height:42)
			HStack {
				Text("newDayAction")
				Spacer()
				Picker(session.alertsNewDay.description, selection: $session.alertsNewDay) {
					ForEach(AffirmDayStartup.allCases, id: \.self) { option in
						Text(option.description)
					}
				}
				.pickerStyle(MenuPickerStyle())
			}
			.frame(height:42)
			Stepper("historyCount\(session.historyCount)", value: $session.historyCount, in: 2...60)
			.frame(height:42)
//			Toggle("shareWarning", isOn: .constant(true))
//			.frame(height:42)
//			Toggle("useiCloud", isOn: .constant(true))
//			.frame(height:42)
#if targetEnvironment(simulator)
			DebugView(session: session, selectedTab: $selectedTab)
#else
			if self.presentingDebugView == 0 {
				EmptyView()
			}
			else if self.presentingDebugView % 3 != 0 {
				Text(self.presentingDebugView.description)
			}
			else {
				DebugView(session: session, selectedTab: $selectedTab)
			}
#endif
		}
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
		SettingsView(session: AffirmationSession(), notifications: AffirmingNotificationIssuer(), selectedTab: .constant(0))
    }
}
