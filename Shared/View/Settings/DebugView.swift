//
//  DebugButton.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/16/21.
//

import SwiftUI

struct AffirmationDocketView: View {
	let docket: AffirmationDocket
    var body: some View {
		VStack {
			HStack {
				Text(docket.today.date.todayIdentifier)
				Spacer()
				Text(docket.today.affirmation.title)
			}
			HStack {
				Text(docket.tomorrow.date.todayIdentifier)
				Spacer()
				Text(docket.tomorrow.affirmation.title)
			}
		}
	}
}

struct DebugView: View {
	@ObservedObject var session: AffirmationSession
    @Binding var selectedTab: Int
	@State private var presentingDebugActionSheet: Bool = false
	
    var body: some View {
		VStack {
		AffirmationDocketView(docket: session.affirmCalendar.docket)
        Button("Debug") {
			self.presentingDebugActionSheet = true
		}
		.actionSheet(isPresented: $presentingDebugActionSheet) {
			ActionSheet(
				title: Text("Warning"),
				message: Text("This is dangerous debug stuff."),
				buttons: [
					.cancel(),
					.default(Text("Files Print")) {
						session.debugHistoryLogs(recreate: false)
					},
					.default(Text("Recreate logs")) {
						self.selectedTab = 1
						session.debugHistoryLogs(recreate: true)
					},
					.destructive(Text("Make today new again")) {
						self.selectedTab = 0
						session.debugHistoryRewrite(removal: .today, days: nil)
					},
					.destructive(Text("Skipped a day")) {
						self.selectedTab = 0
						session.debugHistoryRewrite(removal: .includingYesterday, days: nil)
					},
					.destructive(Text("First Launch")) {
						self.selectedTab = 0
						session.debugHistoryRewrite(removal: .everything, days: nil)
					},
					.destructive(Text("Rewrite today's history")) {
						self.selectedTab = 0
						session.debugHistoryRewrite(removal: .everything, days: 0...30)
					},
				]
			)
		}
		}
	}
}

struct DebugButton_Previews: PreviewProvider {
    static var previews: some View {
		DebugView(session: AffirmationSession(), selectedTab: .constant(0))
    }
}
