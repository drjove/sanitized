//
//  AffirmationDetailView.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/31/21.
//

import SwiftUI

struct AffirmationDetailView: View {
	let affirmation: Affirmation
	
    var body: some View {
		VStack(alignment: .leading, spacing: nil) {
			AffirmCategoryView(category: affirmation.category)
			VStack(alignment: .leading) {
				Text(affirmation.intro)
					.italic()
					.lineLimit(2)
					.multilineTextAlignment(.leading)
				Text(affirmation.title)
					.bold().underline()
					.lineLimit(2)
					.multilineTextAlignment(.leading)
			}
		}
    }
}

struct AffirmationDetailView_Previews: PreviewProvider {
    static var previews: some View {
		AllColorSchemesView {
			AffirmationDetailView(affirmation: Affirmation.allCases[0])
		}
    }
}
