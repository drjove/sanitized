//
//  AffirmCategoryView.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/3/21.
//

import SwiftUI

struct AffirmCategoryView: View {
	let category: AffirmCategory
    var body: some View {
		HStack {
			Image(category.rawValue)
				.resizable()
				.aspectRatio(contentMode: .fit)
				.frame(width: 44, height: 44)
			VStack(alignment: .leading) {
				Text(category.title).bold().lineLimit(2)
				Text(category.question)
			}
		}
    }
}

struct AffirmCategoryView_Previews: PreviewProvider {
    static var previews: some View {
		AllColorSchemesView {
			List(AffirmCategory.allCases, id: \.self) {
				AffirmCategoryView(category: $0)
			}
		}
    }
}
