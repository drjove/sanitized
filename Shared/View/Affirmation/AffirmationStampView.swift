//
//  AffirmationStampView.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/12/21.
//

import SwiftUI

enum AffirmationStickerSize: String {
	case small
	case medium
	case large
}

struct AffirmationStampView: View {
	let affirmation: Affirmation
    var body: some View {
		VStack {
			Image(affirmation.category.rawValue)
				.resizable()
				.aspectRatio(contentMode: .fit)
				.frame(width: 44, height: 44)
			Text(affirmation.title)
				.bold()
				.multilineTextAlignment(.center)
		}
	}
	
	func sticker(size: AffirmationStickerSize) -> some View {
		switch size {
		case .small:
			return self.frame(width: 300, height: 300)
		case .medium:
			return self.frame(width: 408, height: 408)
		case .large:
			return self.frame(width: 618, height: 618)
		}
	}
}

struct AffirmationStampView_Previews: PreviewProvider {
    static var previews: some View {
		AllColorSchemesView {
			AffirmationStampView(affirmation: Affirmation.random)
			}
    }
}
