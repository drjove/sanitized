//
//  AffirmationLineItemView.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/30/21.
//

import SwiftUI

struct AffirmationLineItemView: View {
	let affirmation: Affirmation
	
	var body: some View {
		HStack {
			Image(affirmation.category.rawValue)
				.resizable()
				.aspectRatio(contentMode: .fit)
				.frame(width: 44, height: 44)
			VStack(alignment: .leading, spacing: 0) {
				Text(affirmation.title).bold().lineLimit(2)
				Text(affirmation.intro).fontWeight(.light).italic().truncationMode(.tail).lineLimit(1)
			}
		}
	}
}

struct AffirmationView_Previews: PreviewProvider {
    static var previews: some View {
		AllColorSchemesView {
			List(AffirmCategory.allCases, id: \.self) { c in
				if let a = Affirmation.allCases.first(where: {$0.category==c}) {
					AffirmationLineItemView(affirmation: a)
				}
			}
		}
    }
}
