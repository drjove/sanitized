//
//  AboutView.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/31/21.
//

import SwiftUI

struct AboutView: View {
    var body: some View {
		VStack {
			HStack {
				Text(Bundle.main.displayName)
				Text(Bundle.main.versionNumber)
			}
			AttributedTextView(text: NSAttributedString(bundleHtml: "About")!)
			Spacer()
		}.padding(8)
    }
}

struct AboutView_Previews: PreviewProvider {
    static var previews: some View {
		AllColorSchemesView {
			AboutView()
		}
    }
}
