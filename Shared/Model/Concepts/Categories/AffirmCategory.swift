//
//  AffirmCategory.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/30/21.
//

import Foundation

enum AffirmCategory: String, Hashable, CaseIterable {
	case innerSelf
	case strengths
	case home
	case job
}

extension AffirmCategory {
	var title: String {
		NSLocalizedString(self.rawValue, tableName: "AffirmCategory_title", comment: self.rawValue)
	}
	
	var question: String {
		NSLocalizedString(self.rawValue, tableName: "AffirmCategory_question", comment: self.rawValue)
	}
}
