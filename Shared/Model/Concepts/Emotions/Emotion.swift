//
//  Emotion.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/31/21.
//

import Foundation

enum Emotion: String, Codable, Hashable, CaseIterable {
	case confident
	case happy
	case present
	case thoughtful
	
	case flat
	case muted
	case onEdge
	
	case sad
	case anxious
	case angry
	case depressed
}

extension Emotion {
	var title: String {
		NSLocalizedString(self.rawValue, tableName: "Emotion_title", comment: self.rawValue)
	}
	
	var emoticon: String {
		NSLocalizedString(self.rawValue, tableName: "Emotion_emoticon", comment: self.rawValue)
	}
	
	static var enspecifiedEmoticon: String {
		NSLocalizedString("unspecified", tableName: "Emotion_emoticon", comment: "□")
	}
	
	static var historyEmoticon: String {
		NSLocalizedString("history", tableName: "Emotion_emoticon", comment: "□")
	}
}
