//
//  Affirmation.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/30/21.
//

import Foundation

struct Affirmation: Codable, Hashable, CaseIterable {
	let id: Int
	let title: String
	let intro: String
	let category: AffirmCategory
	
	static var random: Affirmation {
		Self.allCases.randomElement()!
	}

	private init(_ id: Int, _ title: String, _ intro: String, _ category: AffirmCategory) {
		self.id = id
		self.title = title
		self.intro = intro
		self.category = category
	}
	
	init(from decoder: Decoder) throws {
		let container = try decoder.singleValueContainer()
		let id = try container.decode(Int.self)
		self = Self.allCases.first{$0.id == id} ?? Self.allCases[0]
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.singleValueContainer()
		try container.encode(self.id)
	}
	
	func hash(into hasher: inout Hasher) {
		id.hash(into: &hasher)
	}
	
	static var redacted: Affirmation {
		Affirmation(-1, "Think positive", "Striving for joy", .innerSelf)
	}
	
	static let messageFormCases: [Affirmation] = []
	
	static let allCases = [
		Affirmation(410, "I deserve to be happy.", "Yes I do", .innerSelf),
		Affirmation(420, "I deserve to have joy.", "Joy is good", .innerSelf),
		Affirmation(430, "I deserve to have fun.", "Even if there are things I haven't finished", .innerSelf),
		
		Affirmation(480, "It is OK to feel sad.", "", .innerSelf),
		
		Affirmation(710, "I am a good person.", "", .strengths),
		Affirmation(730, "I am smart.", "", .strengths),
		Affirmation(750, "I am generous.", "", .strengths),
		
		Affirmation(800, "I create a wonderful home.", "even if the cats shit in it", .home),
		Affirmation(810, "I am a good cook.", "", .home),
		Affirmation(830, "I am organized.", "So much more so than I ever dreamed possible", .home),
		
		Affirmation(100, "I am exceptional at my job.", "", .job),
	]
}
