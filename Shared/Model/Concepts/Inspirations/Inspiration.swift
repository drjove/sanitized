//
//  Inspiration.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/31/21.
//

import Foundation

enum Inspiration: String, Codable, Hashable, CaseIterable {
	case neely
	case stuart
	case ross
	case roosevelt
	case gabriel
	case hopper
	
	static var random: Inspiration {
		Self.allCases.randomElement()!
	}
}

extension Inspiration: CustomStringConvertible {
	var description: String {
		NSLocalizedString(self.rawValue, tableName: "Inspiration", comment: self.rawValue)
	}
}
