//
//  AffirmingFully.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/30/21.
//

import Foundation

struct AffirmingFully: Codable, Hashable {
	var today: AffirmingToday
	var list: AffirmingList
	
	init(maxAffirmations: Int, prime: Affirmation? = nil, date: Date = Date()) {
		self.today = AffirmingToday(date: date)
		self.list = AffirmingList(max: maxAffirmations, prime: prime)
	}
	
	var isToday: Bool {
		self.todayIdentifier == Date().todayIdentifier && list.isEmpty == false
	}
	
	var todayIdentifier: String {
		return today.date.todayIdentifier
	}
	
	var emotedCount: Int {
		today.emotedCount + list.emotedCount
	}
	
	var thoughtWordCount: Int {
		today.thoughtWordCount + list.thoughtWordCount
	}
}
