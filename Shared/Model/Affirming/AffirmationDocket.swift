//
//  AffirmationDocket.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/20/21.
//

import Foundation

struct AffirmationDocket: Codable {
	let today: AffirmingNotification
	let tomorrow: AffirmingNotification
	
	init() {
		var affirms = Set(Affirmation.allCases)
		self.today = AffirmingNotification(
			affirmation: affirms.randomElement()!, days: 0)
		affirms.remove(self.today.affirmation)
		self.tomorrow = AffirmingNotification(
			affirmation: affirms.randomElement()!, days: 1)
	}
	
	init(_ today: AffirmingNotification, tomorrow: AffirmingNotification) {
		self.today = today
		self.tomorrow = tomorrow
	}
	
	func future(timeOfDay: DateComponents) -> AffirmingNotification {
		let today = self.today.date
		let calendar = Calendar.current
		let signal = calendar.date(byAdding: timeOfDay, to: today)!
		let moment = Date()
		let compare = calendar.compare(moment, to: signal, toGranularity: Calendar.Component.second)
		return compare == .orderedAscending ? self.today : self.tomorrow
	}
	
	func startAgain() -> AffirmationDocket {
		var affirms = Set(Affirmation.allCases)
		affirms.remove(self.today.affirmation)
		let today = AffirmingNotification(
			affirmation: affirms.randomElement()!, days: 0)
		affirms.remove(self.today.affirmation)
		let tomorrow = AffirmingNotification(
			affirmation: affirms.randomElement()!, days: 1)
		return AffirmationDocket(today, tomorrow: tomorrow)
	}
	
	func verifyDay(for day: Date = Date()) -> (AffirmationDocket, Int) {
		let maybeToday = self.today
		let thinkingTomorrow = self.tomorrow
		let today = Calendar.current.startOfDay(for: day)
		if maybeToday.date == today {
			return (self, 0)
		}
		else if thinkingTomorrow.date == today {
			let affirms = Set(Affirmation.allCases).subtracting([
				maybeToday.affirmation, thinkingTomorrow.affirmation
			])
			let today = thinkingTomorrow
			let tomorrow = AffirmingNotification(
				affirmation: affirms.randomElement()!, days: 1)
			return (AffirmationDocket(today, tomorrow: tomorrow), 1)
		}
		else {
			var affirms = Set(Affirmation.allCases)
			let today = AffirmingNotification(
				affirmation: affirms.randomElement()!, days: 0)
			affirms.remove(self.today.affirmation)
			let tomorrow = AffirmingNotification(
				affirmation: affirms.randomElement()!, days: 1)
			return (AffirmationDocket(today, tomorrow: tomorrow), 2)
		}
	}
	
	func debugReset(scope: AffirmationStorageRemovalScope) -> AffirmationDocket {
		switch scope {
		case .none:
			return self
		case .today:
			return self.verifyDay(for: Calendar.current.date(byAdding: .day, value: -1, to: Date())!).0
		case .includingYesterday:
			return self.verifyDay(for: Calendar.current.date(byAdding: .day, value: -2, to: Date())!).0
		case .everything:
			return startAgain()
		}
	}
}
