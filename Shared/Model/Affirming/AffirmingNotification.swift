//
//  AffirmingNotification.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/12/21.
//

import Foundation

struct AffirmingNotification: Codable {
    let date: Date
    let affirmation: Affirmation
    
    static let scheme = "affirmwidget"
    static let kind = "com.softwarebyjove.Daily-Affirmation.AffirmationWidget"
    
    init(affirmation: Affirmation = Affirmation.random, days: Int = 0) {
		let cal = Calendar.current
		var midnight = Date()
		midnight = cal.startOfDay(for: midnight)
		midnight = cal.date(byAdding: .day, value: days, to: midnight)!
		self.date = midnight
		self.affirmation = affirmation
    }
}
