//
//  AffirmingToday.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/31/21.
//

import Foundation

struct AffirmingToday: Codable, Hashable {
	let date: Date
	let inspiration: Inspiration
	var emotion: Emotion?
	var thoughts: String
	
	init(date: Date = Date()) {
		self.date = date
		self.inspiration = Inspiration.random
		self.emotion = nil
		self.thoughts = ""
	}
	
	var emotedCount: Int {
		emotion == nil ? 0 : 1
	}
	
	var thoughtWordCount: Int {
		thoughts.wordCount
	}
}
