//
//  AffirmingLogEntry.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/8/21.
//

import Foundation

struct AffirmingLogEntry: Codable, Hashable {
	let identifier: String
	let dayEmotion: Emotion?
	let dayWordCount: Int
	let affirmationCount: Int
	let entryEmotedCount: Int
	let entryWordCount: Int
	
	init(_ fully: AffirmingFully) {
		self.identifier = fully.todayIdentifier
		self.dayEmotion = fully.today.emotion
		self.dayWordCount = fully.today.thoughtWordCount
		self.affirmationCount = fully.list.endIndex
		self.entryEmotedCount = fully.list.emotedCount
		self.entryWordCount = fully.list.thoughtWordCount
	}
}
