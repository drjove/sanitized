//
//  AffirmingEntry.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/30/21.
//

import Foundation

struct AffirmingEntry: Hashable, Codable  {
	let affirmation: Affirmation
	var emotion: Emotion?
	var thoughts: String
	
	init(_ affirmaton: Affirmation) {
		self.affirmation = affirmaton
		self.emotion = nil
		self.thoughts = ""
	}
	
	var emotedCount: Int {
		emotion == nil ? 0 : 1
	}
	
	var thoughtWordCount: Int {
		thoughts.wordCount
	}
}
