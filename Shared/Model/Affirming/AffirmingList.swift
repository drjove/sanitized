//
//  AffirmingList.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 3/31/21.
//

import Foundation

struct AffirmingList: RandomAccessCollection, Codable, Hashable {
	typealias Base = [AffirmingEntry]
    typealias Index = Base.Index
    typealias Element = (index: Index, entry: Base.Element)
	
	private var entries: Base = []
	
	init(max: Int, prime: Affirmation? = nil) {
		var affirmations = Array(Affirmation.allCases.random(count: max, unique: true))
		if let prime = prime {
			if let index = affirmations.firstIndex(of: prime) {
				affirmations[index] = affirmations[0]
			}
			affirmations[0] = prime
		}
		self.entries = affirmations.map(AffirmingEntry.init)
	}
	
	@discardableResult
	mutating func add() -> AffirmingEntry? {
		let all = Set<Affirmation>(Affirmation.allCases)
		let has = entries.map(\.affirmation)
		let options = all.subtracting(has)
		if let new = options.randomElement() {
			let entry = AffirmingEntry(new)
			self.entries.append(entry)
			return entry
		}
		return nil
	}
	
	var startIndex: Index {
		entries.startIndex
	}
	
	var endIndex: Index {
		entries.endIndex
	}
	
	subscript(index: Int) -> Element {
		get {
			return (index, entries[index])
		}
		set {
			entries[index] = newValue.entry
		}
    }
	
	var emotedCount: Int {
		entries.reduce(0) {$0 + $1.emotedCount}
	}
	
	var thoughtWordCount: Int {
		entries.reduce(0) {$0 + $1.thoughtWordCount}
	}
}
