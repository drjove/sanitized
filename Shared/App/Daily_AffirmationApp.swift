//
//  Daily_AffirmationApp.swift
//  Shared
//
//  Created by David Giovannini on 3/30/21.
//

import Combine
import SwiftUI
import WidgetKit

// Features
//TODO: history full day read/only viewer
//TODO: share buttons

// Integration
//TODO: UNNotificationContentExtension
//TODO: iMesaage stickies
//TODO: watch app/complication

// Adaptability
//TODO: accessability
//TODO: large screen layout

// Theme
//TODO: iconography
//TODO: colorization of things

// Long Term
//TODO: migrate from local storage to cload storage on change detection
//TODO: option for local only storage
//TODO: Concept Schemas
//TODO: Title as concept
//TODO: handle decoding user settable unknown category enum values well
//TODO: affirmation distribution enum (none, random, fixed, per category, named set) selection and weekday preset
//TODO: Concepts become not hardcoded
//TODO: Rethink log storage/debug listings
//TODO: Move to full cloud kit database


@main
struct Daily_AffirmationApp: App {
	@Environment(\.scenePhase) var scenePhase
	@ObservedObject var session: AffirmationSession
	@ObservedObject var notifications: AffirmingNotificationIssuer
    @State private var selectedTab: Int = 0
    
    var widgetNotify: AnyCancellable?
    
    init() {
		let storage = AffirmationDocumentsStorage()
		self.session = AffirmationSession(storage: storage)
		let notifications = AffirmingNotificationIssuer()
		self.notifications = notifications
		
		self.widgetNotify = self.session.affirmCalendar.$docket.sink { docket in
			notifications.docket = docket
			WidgetCenter.shared.reloadTimelines(ofKind: AffirmingNotification.kind)
		}
    }
    
    var body: some Scene {
        WindowGroup {
			AffirmationLoadingView(session: session, notifications: notifications, selectedTab: $selectedTab)
			.onOpenURL { url in
				guard url.scheme == AffirmingNotification.scheme else { return }
				self.selectedTab = 0
			}
			.onChange(of: notifications.notification) { response in
				self.selectedTab = 0
			}
		}
        .onChange(of: scenePhase) { newScenePhase in
			switch newScenePhase {
			case .inactive:
				session.inactive()
			case .background:
				break
			case .active:
				session.activated()
				notifications.activated()
			@unknown default:
				break
			}
		}
	}
}
