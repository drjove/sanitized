//
//  UserDefaults+Affirming.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/13/21.
//

import Foundation

extension UserDefaults {
	static var affirming: UserDefaults {
		UserDefaults(suiteName: "group.com.softwarebyjove.Daily-Affirmation")!
	}
}
