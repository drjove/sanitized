//
//  Date+todayIdentifier.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/13/21.
//

import Foundation

fileprivate let identifierFormatter: DateFormatter = {
	let formatter = DateFormatter()
	formatter.dateFormat = "YYYY-MM-dd"
	return formatter
}()

extension Date {
	var todayIdentifier: String {
		identifierFormatter.string(from: self)
	}
}

extension String {
	var todayIdentifierDate: Date {
		identifierFormatter.date(from: self)!
	}
}
