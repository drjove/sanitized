//
//  AffirmationCalendar.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/12/21.
//

import Foundation
import os.log

class AffirmationCalendar: ObservableObject {
	private static let log = OSLog("calendar")
	
	@UserDefault("affirmationDocket", UserDefaultMapperCodable(), UserDefaults.affirming, defaultValue: {AffirmationDocket()})
	private static var _docket
	@Published
	private(set) var docket: AffirmationDocket
	
	init(verify: Bool) {
		self.docket = Self._docket
		if verify {
			self.verifyDay()
		}
	}
	
	func startAgain() {
		let newValue = self.docket.startAgain()
		Self._docket = newValue
		self.docket = newValue
	}
	
	func verifyDay(for day: Date = Date()) {
		let newValue = self.docket.verifyDay(for: day)
		Self._docket = newValue.0
		self.docket = newValue.0
		let affirmation = newValue.0.today.affirmation
		if newValue.1 > 0 {
			os_log("Today<%d> is: [%d]%s ", log: Self.log, type: .info, newValue.1, affirmation.id, affirmation.title)
		}
	}
	
	func debugReset(scope: AffirmationStorageRemovalScope) {
		let newValue = self.docket.debugReset(scope: scope)
		Self._docket = newValue
		self.docket = newValue
	}
}
