//
//  AffirmationSession.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/1/21.
//

import Foundation
import Combine

class AffirmationSession: ObservableObject {
	private let storage: AffirmationDocumentsStorage?
	private var saving: AnyCancellable?
	
	private(set) var affirmCalendar: AffirmationCalendar
	
	@Published var fully: AffirmingFully {
		didSet {
			let count = history.count
			if count > 0 {
				history[count-1] = AffirmingLogEntry(fully)
			}
			else  {
				history = [AffirmingLogEntry(fully)]
			}
		}
	}
	@Published private(set) var loading: Bool
	@Published private(set) var newDayTrigger: Bool
	@Published private(set) var history: [AffirmingLogEntry]
	
	@UserDefault("maxAffirmations", UserDefaultMapperAny(), defaultValue: 5)
	private static var _maxAffirmations
	@Published var maxAffirmations: Int {
		didSet {
			Self._maxAffirmations = maxAffirmations
		}
	}
	
	@UserDefault("alertsNewDay", UserDefaultMapperRaw(), defaultValue: AffirmDayStartup.startNew)
	private static var _alertsNewDay
	@Published var alertsNewDay: AffirmDayStartup {
		didSet {
			Self._alertsNewDay = alertsNewDay
		}
	}
	
	@UserDefault("historyCount", UserDefaultMapperAny(), defaultValue: 30)
	private static var _historyCount
	@Published var historyCount: Int {
		didSet {
			Self._historyCount = historyCount
			self.history = storage?.history(max: self.historyCount) ?? []
		}
	}
	
	convenience init() {
		self.init(storage: nil)
	}

	init(storage: AffirmationDocumentsStorage?) {
		self.storage = storage
		self.affirmCalendar = AffirmationCalendar(verify: false)
		self.maxAffirmations = Self._maxAffirmations
		self.historyCount = Self._historyCount
		self.alertsNewDay = Self._alertsNewDay
		
		self.newDayTrigger = false
		
		if storage != nil {
			self.fully = AffirmingFully(maxAffirmations: 0)
			self.history = []
			self.loading = true
		}
		else {
			let fully = AffirmingFully(maxAffirmations: Self._maxAffirmations)
			self.fully = fully
			self.history = [AffirmingLogEntry(fully)]
			self.loading = false
		}
	}
	
	func activated() {
		affirmCalendar.verifyDay()
		if self.loading == true {
			initialRead()
		}
		else {
			verifyDay()
		}
	}
	
	private func initialRead() {
		if let storage = storage {
			DispatchQueue.global().async {
				let read = storage.read()
				DispatchQueue.main.async {
					self.history = self.storage?.history(max: self.historyCount) ?? []
					self.fully = read ?? AffirmingFully(maxAffirmations: Self._maxAffirmations, prime: self.affirmCalendar.docket.today.affirmation)
					self.loading = false
					self.verifyDay()
					self.saving = self.$fully.debounce(for: .seconds(storage.throttle), scheduler: DispatchQueue.global()).sink(receiveValue: storage.write)
				}
			}
		}
	}
	
	private func verifyDay() {
		// if read yesterday || reactivated with resterday
		if self.fully.isToday == false {
			if Self._alertsNewDay == .alert {
				self.newDayTrigger = true
			}
			else if Self._alertsNewDay == .startNew {
				self.startAgain(agreed: true, resetPrime: false)
			}
			// User has to press "Start Again" button
		}
	}
	
	public func startAgain(agreed: Bool, resetPrime: Bool) {
		self.newDayTrigger = false
		if resetPrime {
			self.affirmCalendar.startAgain()
		}
		if agreed {
			self.saveNow()
			self.fully = AffirmingFully(maxAffirmations: Self._maxAffirmations, prime: affirmCalendar.docket.today.affirmation)
		}
	}
	
	func inactive() {
		saveNow()
	}
	
	func debugHistoryLogs(recreate: Bool) {
		if recreate {
			self.storage?.recreateHistoryLogs()
			self.history = self.storage?.history(max: self.historyCount) ?? []
		}
		self.storage?.allFiles().forEach({print($0.lastPathComponent)})
	}
	
	func debugHistoryRewrite(removal scope: AffirmationStorageRemovalScope, days: ClosedRange<Int>?) {
		if scope != .none {
			self.fully = AffirmingFully(maxAffirmations: 0)
		}
		self.affirmCalendar.debugReset(scope: scope)
		self.storage?.remove(scope: scope)
		if let days = days {
			self.storage?.backfillHistory(days: days, count: self.maxAffirmations, prime: self.affirmCalendar.docket.today)
		}
		DispatchQueue.main.async {
			self.initialRead()
		}
	}
	
	private func saveNow() {
		self.storage?.write(fully)
	}
}
