//
//  AffirmationStorage.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/1/21.
//

import Foundation
import os.log

class AffirmationDocumentsStorage {
	private static let log = OSLog("storage")
	private let encoder = JSONEncoder()
	private let decoder = JSONDecoder()
	
	private let version = "1.0.1"
	private let fileManager = FileManager.default
	private var lastWriteHash: Int = 0
	
	let throttle: TimeInterval = 5.0
	
	private lazy var baseURL: URL = {
		let cloud = fileManager.url(forUbiquityContainerIdentifier: nil)?.appendingPathComponent("Documents/\(version)")
		if let cloud = cloud {
			if !fileManager.fileExists(atPath: cloud.path, isDirectory: nil) {
				do {
					try fileManager.createDirectory(at: cloud, withIntermediateDirectories: true, attributes: nil)
					os_log("New Cloud: %s", log: Self.log, type: .info, cloud.absoluteString)
					return cloud
				}
				catch {
					os_log("%s", log: Self.log, type: .error, error.localizedDescription)
				}
			}
			else {
				os_log("Existing Cloud: %s", log: Self.log, type: .info, cloud.absoluteString)
				return cloud
			}
		}
        let local = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("\(version)")
        try? fileManager.createDirectory(at: local, withIntermediateDirectories: true, attributes: nil)
        os_log("Local: %s", log: Self.log, type: .info, local.absoluteString)
        return local
	}()
	
	func allFiles(allVersions: Bool = true, matching: (URL)->Bool = {_ in true}) -> [URL] {
		let url = allVersions ? self.baseURL.deletingLastPathComponent() : self.baseURL
		let enumerator = fileManager.enumerator(at: url, includingPropertiesForKeys: nil)
		var urls: [URL] = []
		for case let path as URL in enumerator! {
			if matching(path) {
				urls.append(path)
			}
		}
		urls.sort(by: {$0.absoluteString < $1.absoluteString})
		return urls
	}
	
	func read() -> AffirmingFully? {
		let today = Date()
		if let todays = read(date: today) {
			return todays
		}
		let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: today)!
		return read(date: yesterday)
	}
	
	func read(date: Date) -> AffirmingFully? {
		let id = date.todayIdentifier
		let path = baseURL.appendingPathComponent(id).appendingPathExtension("json")
		do {
			let data = try Data(contentsOf: path)
			let size = data.count
			let result = try decoder.decode(AffirmingFully.self, from: data)
			lastWriteHash = result.hashValue
			os_log("Read: %s [%d] [%d]", log: Self.log, type: .info, path.lastPathComponent, lastWriteHash, size)
			return result
		}
		catch {
			os_log("%s", log: Self.log, type: .error, error.localizedDescription)
		}
		return nil
	}

	func write(_ fully: AffirmingFully) {
		guard fully.list.isEmpty == false else { return }
		let hash = fully.hashValue
		guard hash != lastWriteHash else { return }
		do {
			let id = fully.todayIdentifier
			let json1 = try encoder.encode(fully)
			let size1 = json1.count
			let path1 = baseURL.appendingPathComponent(id).appendingPathExtension("json")
			try json1.write(to: path1, options: [])
			
			let log = AffirmingLogEntry(fully)
			let json2 = try encoder.encode(log)
			let size2 = json2.count
			let path2 = baseURL.appendingPathComponent("\(id)_log").appendingPathExtension("json")
			try json2.write(to: path2, options: [])

			lastWriteHash = hash
			os_log("Write: %s<%d> [%d] [%d]", log: Self.log, type: .info, path1.lastPathComponent, lastWriteHash, size1, size2)
		}
		catch {
			os_log("%s", log: Self.log, type: .error, error.localizedDescription)
		}
	}
}

extension AffirmationDocumentsStorage {
	func history(max: Int) -> [AffirmingLogEntry] {
		let urls = allFiles(matching:{$0.lastPathComponent.hasSuffix("_log.json")}).suffix(max)
		return urls.compactMap { path in
			do {
				let data = try Data(contentsOf: path, options: .uncachedRead)
				let result = try decoder.decode(AffirmingLogEntry.self, from: data)
				return result
			}
			catch {
				os_log("%s", log: Self.log, type: .error, error.localizedDescription)
			}
			return nil
		}
	}
}

extension AffirmationDocumentsStorage {
	func remove(day: Date) {
		let id = day.todayIdentifier
		let patha = baseURL.appendingPathComponent(id).appendingPathExtension("json")
		try? fileManager.removeItem(at: patha)
		let pathb = baseURL.appendingPathComponent("\(id)_log").appendingPathExtension("json")
		try? fileManager.removeItem(at: pathb)
	}

	func remove(scope: AffirmationStorageRemovalScope) {
		switch scope {
		case .none:
			break
		case .today:
			remove(day: Date())
		case .includingYesterday:
			remove(day: Date())
			remove(day: Calendar.current.date(byAdding: .day, value: -1, to: Date())!)
		case .everything:
			self.allFiles(matching: {$0.pathExtension=="json"}).forEach {
				try? fileManager.removeItem(at: $0)
			}
 		}
 	}
	
	func backfillHistory(days: ClosedRange<Int>, count: Int, prime: AffirmingNotification) {
		let cal = Calendar.current
		let today = Date()
		var emotionIndex: Int = Emotion.allCases.randomElement()!.index
		for i in days.reversed() {
			let timeMachine = cal.date(byAdding: .day, value: -i, to: today)!
			emotionIndex += Int.random(in: -1...1)
			if emotionIndex < 0 {
				emotionIndex = 0
			}
			else if emotionIndex >= Emotion.allCases.count {
				emotionIndex = Emotion.allCases.count - 1
			}
			let skipDay = Int.random(in: 1...5)
			if skipDay != 1 {
				let midnight = cal.startOfDay(for: today)
				var affirm = AffirmingFully(maxAffirmations: count, prime: midnight == prime.date ? prime.affirmation : nil, date: timeMachine)
				let skipEmotion = Int.random(in: 1...5)
				if skipEmotion != 1 {
					affirm.today.emotion = Emotion.allCases[emotionIndex]
				}
				self.write(affirm)
			}
		}
	}
	
	func recreateHistoryLogs() {
		allFiles(matching:{$0.lastPathComponent.hasSuffix("_log.json")}).forEach { path in
			try? fileManager.removeItem(at: path)
		}
		allFiles().forEach { path in
			let name = path.lastPathComponent
			let isJSON = name.hasSuffix(".json")
			let isLog = name.hasSuffix("_log.json")
			if isJSON && !isLog {
				do {
					let data = try Data(contentsOf: path, options: .uncachedRead)
					let fully = try decoder.decode(AffirmingFully.self, from: data)
					let log = AffirmingLogEntry(fully)
					let logjson = try encoder.encode(log)
					let logpath = baseURL.appendingPathComponent("\(fully.todayIdentifier)_log").appendingPathExtension("json")
					try logjson.write(to: logpath, options: [])
				}
				catch {
					os_log("%s", log: Self.log, type: .error, error.localizedDescription)
				}
			}
		}
	}
}
