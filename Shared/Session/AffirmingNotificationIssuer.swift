//
//  AffirmingNotificationIssuer.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/10/21.
//

import Foundation
import UserNotifications
import os.log
#if os(iOS)
import BackgroundTasks
import UIKit
#endif
#if os(macOS)
#endif

class AffirmingNotificationIssuer: NSObject, ObservableObject {
	private static let log = OSLog("notifications")
	
	override init() {
		self.timeOfDay = Self._timeOfDay
		super.init()
        registerWithNotificationCenter()
        registerNightlyNotificationUpdateTask()
	}
	
	@Published private(set) var active: Bool?
	
	@UserDefault("timeOfDay", UserDefaultMapperCodable(), defaultValue:
		DateComponents(calendar: Calendar.current, timeZone: .current, hour: 7, minute: 30))
	private static var _timeOfDay
	@Published var timeOfDay: DateComponents {
		didSet {
			Self._timeOfDay = timeOfDay
		}
	}
	@Published var notification: UNNotificationResponse?
	
	var docket = AffirmationDocket() {
		didSet {
			submitUpdateDailyNotificationContent()
		}
	}
	
	func activated() {
		submitNightlyNotificationUpdateTask()
	}
}

extension AffirmingNotification {
	static var categoryIdentifer: String { "dailyNotification" }
	
	var content: UNNotificationContent {
		let content = UNMutableNotificationContent()
		content.title = affirmation.title
		//content.body
		//content.subtitle
		content.categoryIdentifier = AffirmingNotification.categoryIdentifer
		content.userInfo = [
			"affirmation":String(data: try! JSONEncoder().encode(self), encoding: .utf8)!
		]
		return content
	}
}

extension AffirmingNotificationIssuer {
    static var settingsURL: URL {
#if os(iOS)
		URL(string: UIApplication.openSettingsURLString)!
#else
		URL(string: "")!
#endif
    }
    
	var dailyNotificationIdentifier: String {
		Bundle.main.bundleIdentifier! + ".dailyNotification"
	}
	
	private func registerWithNotificationCenter() {
		let center: UNUserNotificationCenter = UNUserNotificationCenter.current()
		center.delegate = self
		let category = UNNotificationCategory(
			identifier: AffirmingNotification.categoryIdentifer,
			actions: [],
			intentIdentifiers: [],
			hiddenPreviewsBodyPlaceholder: "",
			options: [])
		center.setNotificationCategories([category])
	}
	
	private func submitUpdateDailyNotificationContent() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert]) { granted, error in
				if let error = error {
					os_log("%s", log: Self.log, type: .error, error.localizedDescription)
				}
				DispatchQueue.main.async {
					if granted == true {
						self.active = true
						self.updateDailyNotificationContent()
					} else {
						self.active = false
					}
				}
        }
	}
	
	private func updateDailyNotificationContent() {
#if DEBUG1
		let calendar = Calendar.current
		let moment = Date()
		let date = calendar.date(byAdding: .second, value: 30, to: moment)!
		let affirmCalendar = calendar.dateComponents([.hour, .minute, .second], from: date)
        print(affirmCalendar)
#else
		let schedule = self.timeOfDay
#endif
		let request = UNNotificationRequest(
			identifier: self.dailyNotificationIdentifier,
			content: self.docket.future(timeOfDay: schedule).content,
			trigger: UNCalendarNotificationTrigger(dateMatching: schedule, repeats: true))
			
		UNUserNotificationCenter.current().add(request) { (error) in
			if let error = error {
				os_log("%s", log: Self.log, type: .error, error.localizedDescription)
			}
			else {
				os_log("Scheduled Notification on %s", log: Self.log, type: .info, schedule.description)
			}
		}
	}
}

extension AffirmingNotificationIssuer: UNUserNotificationCenterDelegate {
	// User interaction
	func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
		DispatchQueue.main.async {
			self.notification = response
		}
		completionHandler()
	}
	
	// Foreground receive
	func userNotificationCenter(_ center: UNUserNotificationCenter,
                         willPresent notification: UNNotification,
               withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
		// This is a non-event
		completionHandler(UNNotificationPresentationOptions())
	}
}

#if os(iOS)
extension AffirmingNotificationIssuer {
	func registerNightlyNotificationUpdateTask() {
		guard ProcessInfo.isPreview == false else {
			return
		}
		let id = self.dailyNotificationIdentifier
		let hasId = BGTaskScheduler.shared.register(forTaskWithIdentifier: id, using: nil) { task in
			self.handleAddNotification(task)
		 }
		 if hasId == false {
			os_log("Not in PList: %s", log: Self.log, type: .error, id)
		 }
	}

	private func handleAddNotification(_ task: BGTask) {
		DispatchQueue.global().async {
			os_log("Midnight task", log: Self.log, type: .info)
			self.registerNightlyNotificationUpdateTask()
			self.updateDailyNotificationContent()
			task.setTaskCompleted(success: true)
		}
	}
#if DEBUG
	static var simSchedulePrinted = false
#endif
	
	func submitNightlyNotificationUpdateTask() {
		let request = BGAppRefreshTaskRequest(identifier: self.dailyNotificationIdentifier)
		
		var midnight = Date()
		let cal = Calendar.current
		midnight = cal.date(byAdding: .day, value: 1, to: midnight)!
		midnight = cal.startOfDay(for: midnight)
		request.earliestBeginDate = midnight
		
		do {
			try BGTaskScheduler.shared.submit(request)
			os_log("Updating %s on %s", log: Self.log, type: .info, self.dailyNotificationIdentifier, midnight.description)
#if DEBUG
			if Self.simSchedulePrinted == false {
				Self.simSchedulePrinted = true
				print("e -l objc -- (void)[[BGTaskScheduler sharedScheduler] _simulateLaunchForTaskWithIdentifier:@\"\(self.dailyNotificationIdentifier)\"]")
				print("Set break point on this print line and execute the above printed statement in the debugger console.")
			}
#endif
		}
		catch {
			os_log("%s", log: Self.log, type: .error, error.localizedDescription)
		}
	}
}
#endif

#if os(macOS)
extension AffirmingNotificationIssuer {
	func registerNightlyNotificationUpdateTask() {
	}

	func submitNightlyNotificationUpdateTask() {
	}
}
#endif
