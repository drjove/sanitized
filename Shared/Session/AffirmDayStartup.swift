//
//  AffirmDayStartup.swift
//  Daily Affirmation (iOS)
//
//  Created by David Giovannini on 4/18/21.
//

import Foundation

enum AffirmDayStartup: String, CaseIterable, Hashable {
	case previousDay
	case alert
	case startNew
	
	var description: String {
		NSLocalizedString(self.rawValue, comment: self.rawValue)
	}
}
