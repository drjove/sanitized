//
//  AffirmationStorageRemovalScope.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/19/21.
//

import Foundation

enum AffirmationStorageRemovalScope {
	case none
	case today
	case includingYesterday
	case everything
}
