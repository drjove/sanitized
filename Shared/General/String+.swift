//
//  String+.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/13/21.
//

import Foundation

public extension String {
	var wordCount: Int {
		let chararacterSet = CharacterSet.whitespacesAndNewlines.union(.punctuationCharacters)
		let components = self.components(separatedBy: chararacterSet)
		return components.reduce(0) { $1.isEmpty ? $0 : $0 + 1 }
	}
}
