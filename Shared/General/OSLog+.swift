//
//  OSLog+.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/13/21.
//

import Foundation
import os.log

public extension OSLog {
    convenience init(_ category: String) {
		self.init(subsystem: Bundle.main.bundleIdentifier!, category: category)
    }
}
