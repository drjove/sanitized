//
//  AllColorSchemes.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/2/21.
//

import SwiftUI

public struct AllColorSchemesView<Content: View>: View {
	public let content: ()->Content
    public var body: some View {
		ForEach(ColorScheme.allCases, id: \.self) {
			content().preferredColorScheme($0)
		}
	}
}

struct AllColorSchemes_Previews: PreviewProvider {
    static var previews: some View {
        AllColorSchemesView {
			Text("Hello")
        }
    }
}
