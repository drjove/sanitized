//
//  UserDefault.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/2/21.
//

import Foundation
import os.log

public protocol UserDefaultMapper {
	associatedtype WrappedValue
	func mapWrite(_ value: WrappedValue) throws -> Any
	func mapRead(_ value: Any) throws -> WrappedValue
}

public struct TypeMismatchError: Error, CustomStringConvertible {
	public let value: Any
	public let type: Any.Type
	public var description: String {
		"UserDefaults TypeMismatchError"
	}
}

public struct UserDefaultMapperAny<WrappedValue>: UserDefaultMapper {
    public func mapWrite(_ value: WrappedValue) throws -> Any {
		value
    }
    
    public func mapRead(_ value: Any) throws -> WrappedValue {
		if let value = value as? WrappedValue {
			return value
		}
		throw TypeMismatchError(value: value, type: WrappedValue.self)
    }
}

public struct UserDefaultMapperRaw<WrappedValue: RawRepresentable>: UserDefaultMapper {
    public func mapWrite(_ value: WrappedValue) throws -> Any {
		value.rawValue
    }
    
    public func mapRead(_ value: Any) throws -> WrappedValue {
		if let value = value as? WrappedValue.RawValue, let result = WrappedValue(rawValue: value) {
			return result
		}
		throw TypeMismatchError(value: value, type: WrappedValue.self)
    }
}

public struct UserDefaultMapperCodable<WrappedValue: Codable>: UserDefaultMapper {
	private let encoder = JSONEncoder()
	private let decoder = JSONDecoder()

    public func mapWrite(_ value: WrappedValue) throws -> Any {
		try encoder.encode(value)
    }
    
    public func mapRead(_ value: Any) throws -> WrappedValue {
		if let value = value as? Data {
			return try decoder.decode(WrappedValue.self, from: value)
		}
		throw TypeMismatchError(value: value, type: WrappedValue.self)
    }
}

public protocol UserDefaultContainer {
	func object(forKey aKey: String) -> Any?
    func set(_ anObject: Any?, forKey aKey: String)
    func removeObject(forKey aKey: String)
}

extension UserDefaults: UserDefaultContainer {
}

extension NSUbiquitousKeyValueStore: UserDefaultContainer {
}

@propertyWrapper
public struct UserDefault<WrappedValue, Mapper>
		where Mapper: UserDefaultMapper, Mapper.WrappedValue == WrappedValue {
	private static var log: OSLog { OSLog("user defaults") }
    public let key: String
    private let mapper: Mapper
    private let defaultValue: ()->WrappedValue
	private let container: UserDefaultContainer
    
    // If defaultValue can change, you may need to set it
    init(
			_ key: String,
			_ mapper: Mapper,
			_ container: UserDefaultContainer = UserDefaults.standard,
			defaultValue: @escaping ()->WrappedValue) {
		self.key = key
		self.mapper = mapper
		self.container = container
		self.defaultValue = defaultValue
    }
    
    public init(
			_ key: String,
			_ mapper: Mapper,
			_ container: UserDefaultContainer = UserDefaults.standard,
			defaultValue: WrappedValue) {
		self.init(key, mapper, container, defaultValue: {defaultValue})
    }
    
    public var wrappedValue: WrappedValue {
        get {
			if let value = container.object(forKey: key) {
				do {
					return try self.mapper.mapRead(value)
				}
				catch {
					os_log("get '%s' %s", log: Self.log, type: .info, key, error.localizedDescription)
				}
			}
			return defaultValue()
        }
        set {
			do {
				let value = try self.mapper.mapWrite(newValue)
				container.set(value, forKey: key)
			}
			catch {
				os_log("set '%s' %s", log: Self.log, type: .info, key, error.localizedDescription)
			}
        }
    }
    
    public func reset() {
		container.removeObject(forKey: key)
    }
}
