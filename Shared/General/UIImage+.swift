//
//  UIImage+.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 5/17/21.
//

import SwiftUI
import UIKit

extension View {
    func snapshot() -> UIImage {
        let controller = UIHostingController(rootView: self)
        let view = controller.view

        let targetSize = controller.view.intrinsicContentSize
        view?.bounds = CGRect(origin: .zero, size: targetSize)
        view?.backgroundColor = .clear

        let renderer = UIGraphicsImageRenderer(size: targetSize)

        return renderer.image { _ in
            view?.drawHierarchy(in: controller.view.bounds, afterScreenUpdates: true)
        }
    }
}

extension UIImage {
	convenience init?(cached name: String) {
		guard let url = Self.url(cached: name) else { return nil }
		self.init(contentsOfFile: url.path)
	}
	
	func url(cached name: String) -> URL {
		UIImage.url(cached: name, sourced: {_ in self})!
	}
	
	static func url(cached name: String, sourced: (String)->UIImage? = {UIImage(named: $0)}) -> URL? {
		let fileManager = FileManager.default
		let cacheDirectory = fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first!
		var url = cacheDirectory.appendingPathComponent("image-\(name)")
		url = url.appendingPathExtension("png")
		if fileManager.fileExists(atPath: url.path) {
			return url
		}
		else if let image = sourced(name) {
			let pngData = image.pngData()!
			try! pngData.write(to: url)
			return url
		}
		return nil
	}
}
