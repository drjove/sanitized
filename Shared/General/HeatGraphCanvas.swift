//
//  LineGraphView.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/14/21.
//

import SwiftUI

public protocol HeatSpectrumColors {
	var line: Color { get }
	var grid: Color { get }
	var good: Color { get }
	var neutral: Color { get }
	var bad: Color { get }
	var gridThickness: CGFloat { get }
	var lineThickness: CGFloat { get }
	var elipsesRadius: CGFloat { get }
}

extension HeatSpectrumColors {
	var line: Color {Color.black}
	var grid: Color {Color.white.opacity(0.25)}
	var good: Color {Color.green}
	var neutral: Color {Color.gray}
	var bad: Color {Color.red}
	var gridThickness: CGFloat {1.0}
	var lineThickness: CGFloat {2.0}
	var elipsesRadius: CGFloat {2.5}
}

public struct HeatGraphCanvas: View {
	public let colors: HeatSpectrumColors
    public let dataPoints: [CGFloat?]
    public let yInterval: Int
    
	public var body: some View {
		ZStack {
			HeatGraphView(colors: colors, dataPoints: dataPoints)
			GridGraphShape(intervals: (x: dataPoints.count, y: yInterval))
				.stroke(
					colors.grid,
					lineWidth: colors.gridThickness)
			LineGraphShape(elipsesRadius: colors.elipsesRadius, dataPoints: dataPoints)
				.stroke(
					colors.line,
					lineWidth: colors.lineThickness)
		}
	}
}

public struct HeatGraphView: View {
	public let colors: HeatSpectrumColors
    public let dataPoints: [CGFloat?]
    
	public var body: some View {
		ZStack {
			Rectangle().fill(colors.neutral)
			LinearGradient(
				gradient:
					Gradient(stops: dataPoints.enumerated().compactMap { iter in
						if let value = iter.element {
							let pos = CGFloat(iter.offset) / CGFloat(dataPoints.count)
							let color: Color
							let intensity = Double(value)
							if value < 0.5 {
								let opacity = 1.0 - (intensity * 2.0)
								color = colors.bad.opacity(opacity)
							}
							else if value > 0.5 {
								let opacity = (intensity - 0.5) * 2.0
								color = colors.good.opacity(opacity)
							}
							else {
								color = colors.neutral
							}
							return Gradient.Stop(color: color, location: pos)
						}
						return nil
					}),
				startPoint: .leading,
				endPoint: .trailing)
		}
	}
}

public struct GridGraphShape: Shape {
    public let intervals: (x:Int, y:Int)
    
    public func path(in rect: CGRect) -> Path {
        Path { p in
			let hSpacing: CGFloat
			let hCount: Int
			if intervals.x < 2 {
				hSpacing = rect.width
				hCount = 2
			}
			else {
				hSpacing = rect.width / CGFloat(intervals.x - 1)
				hCount = intervals.x
			}
			for index in 0..<hCount {
				let offset = CGFloat(index) * hSpacing
				p.move(to: CGPoint(x: offset, y: 0))
				p.addLine(to: CGPoint(x: offset, y: rect.size.height))
			}
			let vSpacing: CGFloat
			let vCount: Int
			if intervals.y < 2 {
				vSpacing = rect.height
				vCount = 2
			}
			else {
				vSpacing = rect.height / CGFloat(intervals.y - 1)
				vCount = intervals.y
			}
			for index in 0..<vCount {
				let offset = CGFloat(index) * vSpacing
				p.move(to: CGPoint(x: 0, y: offset))
				p.addLine(to: CGPoint(x: rect.size.width, y: offset))
			}
		}
    }
}

public struct LineGraphShape: Shape {
	public let elipsesRadius: CGFloat
    public let dataPoints: [CGFloat?]

    public func path(in rect: CGRect) -> Path {
        func point(at ix: Int) -> (x: CGFloat, y: CGFloat?) {
            let point = dataPoints[ix]
            let x: CGFloat
			if dataPoints.count > 1 {
				x = rect.width * CGFloat(ix) / CGFloat(dataPoints.count - 1)
			}
			else {
				x = 0
			}
			if let point = point {
				let y = (1.0-point) * rect.height
				return (x: x, y: y)
			}
			return (x: x, y: nil)
        }

        return Path { p in
            guard dataPoints.isEmpty == false else { return }
            var inited = false
            for idx in dataPoints.indices {
				let pnt = point(at: idx)
                if let y = pnt.y {
					if inited == false {
						p.move(to: CGPoint(x: pnt.x, y: y))
						inited = true
					}
					else {
						p.addLine(to: CGPoint(x: pnt.x, y: y))
					}
					if elipsesRadius > 0.0 {
						p.addEllipse(in: CGRect(
							x: pnt.x-elipsesRadius,
							y: y-elipsesRadius,
							width: elipsesRadius*2.0,
							height: elipsesRadius*2.0),
								transform: .identity)
					}
				}
            }
        }
    }
}

fileprivate struct GraphColors: HeatSpectrumColors {
}

struct LineGraphView_Previews: PreviewProvider {
    static var previews: some View {
		HeatGraphCanvas(colors: GraphColors(), dataPoints: [
			0.3,
			0.6,
			0.6,
			nil,
			0.3,
		], yInterval: 6)
    }
}
