//
//  Bundle+.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/1/21.
//

import Foundation

public extension Bundle {
	var displayName: String {
		return (object(forInfoDictionaryKey: "CFBundleDisplayName") as? String ??
			object(forInfoDictionaryKey: "CFBundleName") as? String)!
	}
	
	var releaseVersionNumber: String {
        return infoDictionary?["CFBundleShortVersionString"] as! String
    }
    
    var buildVersionNumber: String {
        return infoDictionary?["CFBundleVersion"] as! String
    }
    
    var versionNumber: String {
		"\(releaseVersionNumber).\(buildVersionNumber)"
    }
}
