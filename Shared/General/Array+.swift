//
//  Array+.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/20/21.
//

import Foundation

public extension Array {
	func random(count: Int, unique: Bool) -> [Element] {
		guard !self.isEmpty else { return [] }
		if unique {
			return Array(self.shuffled().prefix(count))
		}
		return (0..<count).reduce([]) { accume, _ in
			accume + [self.randomElement()!]
		}
	}
}

public extension CaseIterable where Self: Equatable {
	var index: AllCases.Index {
		Self.allCases.firstIndex { $0 == self }!
	}
}
