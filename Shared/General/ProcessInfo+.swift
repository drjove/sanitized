//
//  ProcessInfo+.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/13/21.
//

import Foundation

public extension ProcessInfo {
	static var isPreview: Bool {
		ProcessInfo.processInfo.environment["XCODE_RUNNING_FOR_PREVIEWS"] == "1"
	}
}
