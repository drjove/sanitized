//
//  AttributedTextView.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/3/21.
//

import SwiftUI

public extension NSAttributedString {
	convenience init?(html data: Data) {
		try? self.init(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
	}
	
	convenience init?(bundleHtml name: String) {
		if let filepath = Bundle.main.url(forResource: name, withExtension: "html") {
			if let contents = try? Data(contentsOf: filepath) {
				self.init(html: contents)
				return
			}
		}
		return nil
	}
	
	fileprivate static func mock(_ string: String) -> NSAttributedString {
		let content = NSMutableAttributedString(string: string)
		let paraStyle = NSMutableParagraphStyle()
		paraStyle.alignment = .justified
		paraStyle.lineHeightMultiple = 1.25
		paraStyle.lineBreakMode = .byTruncatingTail
		paraStyle.hyphenationFactor = 1.0

		content.addAttribute(.paragraphStyle,
							 value: paraStyle,
							 range: NSMakeRange(0, string.count))

#if os(iOS)
		let font = UIFont.systemFont(ofSize: 40, weight: .bold)
#endif
#if os(macOS)
		let font = NSFont.systemFont(ofSize: 40, weight: .bold)
#endif
		// First letter/word
		content.addAttributes([
			.font      : font,
			.expansion : 0,
			.kern      : -0.2
		], range: NSMakeRange(0, 1))

		return content
	}
}

public struct AttributedTextView: View {
    public let text: NSAttributedString
    @State private var height: CGFloat = .zero

    public var body: some View {
        InternalLabelView(text: text, dynamicHeight: $height)
            .frame(height: height)
    }
}

struct AttributedLabel_Previews: PreviewProvider {
    static var previews: some View {
		AttributedTextView(text: NSAttributedString.mock("Hello"))
    }
}

fileprivate struct InternalLabelView {
	var text: NSAttributedString
	@Binding var dynamicHeight: CGFloat
}

#if os(iOS)
extension InternalLabelView: UIViewRepresentable {
	func makeUIView(context: Context) -> UILabel {
		let label = UILabel()
		label.numberOfLines = 0
		label.lineBreakMode = .byWordWrapping
		label.textAlignment = .justified
		label.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
		label.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
		return label
	}

	func updateUIView(_ view: UILabel, context: Context) {
		view.attributedText = text
		DispatchQueue.main.async {
			dynamicHeight = view.sizeThatFits(CGSize(width: view.bounds.width, height: CGFloat.greatestFiniteMagnitude)).height
		}
	}
}
#endif

#if os(macOS)
extension InternalLabelView: NSViewRepresentable {
	func makeNSView(context: Context) -> NSTextField {
		let label = NSTextField()
		label.isBezeled = false
		label.drawsBackground = false
		label.isEditable = false
		label.isSelectable = false
		label.lineBreakMode = .byWordWrapping
		label.allowsEditingTextAttributes = true;
		label.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
		label.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
		return label
	}

	func updateNSView(_ view: NSTextField, context: Context) {
		view.attributedStringValue = text
		DispatchQueue.main.async {
			dynamicHeight = view.sizeThatFits(CGSize(width: view.bounds.width, height: CGFloat.greatestFiniteMagnitude)).height
		}
	}
}
#endif
