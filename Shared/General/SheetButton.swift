//
//  SheetButton.swift
//  Daily Affirmation
//
//  Created by David Giovannini on 4/10/21.
//

import SwiftUI

public struct SheetButton<Data, Content: View> : View {
	@Binding private var data: Data
	private let isEmpty: KeyPath<Data, Bool>
	private let content: (Binding<Data>, Binding<Bool>)->Content
	@State private var isPresented: Bool = false
	
	public init(
		_ data: Binding<Data>,
		isEmpty: KeyPath<Data, Bool>,
		@ViewBuilder content: @escaping (Binding<Data>, Binding<Bool>)->Content) {
		self._data = data
		self.isEmpty = isEmpty
		self.content = content
	}
	
	public var body: some View {
		Button(action: {
			self.isPresented = true
		}) {
			Image(systemName: data[keyPath: isEmpty] ? "chevron.right.2" : "chevron.forward").foregroundColor(.accentColor)
		}
		.sheet(isPresented: $isPresented) {
			content($data, $isPresented)
		}
	}
}

struct SheetButton_Previews: PreviewProvider {
    static var previews: some View {
		VStack {
			SheetButton(.constant(true), isEmpty: \.self) { _, _ in
				Text("")
			}
			SheetButton(.constant(false), isEmpty: \.self) { _, _ in
				Text("")
			}
		}
    }
}
